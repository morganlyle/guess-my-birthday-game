from random import randint

name_string = input("Hi! What is your name? ")
name = str(name_string)


for guess_number in range(1, 6):
    guess_year = randint(1924, 2004)
    guess_month = randint(1, 12)

    print("Guess", guess_number, ": ",  name_string, " were you born in: ",
          guess_month, "/", guess_year, "?")

    response = input("Yes or no? ")
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("Whatever I give up :( ")
        exit()
    else:
        print("Drat! Let me try again! ")
